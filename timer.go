package gomodule

import "time"

func WhatTimeIsIt(format string) string {
	return time.Now().Format(format)
}
